Files in the 01_source_data folder were created from multiple sources, and often involved manual cleaning. Details of these source data files, including links to primary data sources, are outlined below.

### closure_proposals.xlsx
This file contains entries for every wiki closure proposal, including closure outcomes.
* Source: [meta:Proposals_for_closing_projects/Closed_proposals](https://meta.wikimedia.org/wiki/Proposals_for_closing_projects#Closed_proposals)
* Source: [meta:Proposals_for_closing_projects/Archive](https://meta.wikimedia.org/wiki/Proposals_for_closing_projects/Archive)

### incubator_test_wikis_status_rejected.tsv
This file contains entries for every rejected request for a new test wiki in Incubator.
* Source: [Category:Incubator:Test_wikis/status/rejected](https://incubator.wikimedia.org/wiki/Category:Incubator:Test_wikis/status/rejected)
* Manually copied (with plans to automate scraping)

### incubator_wikis.tsv
This file contains a list of test wikis in development on the Wikimedia Incubator that meet one or both of the following criteria: they are substantial (having at least 25 mainspace pages); and/or they are active (having some active content creation since the beginning of last calendar year).

The list contains the project's Full_name (e.g., "Wikibooks Javanese (wb/jv)") as it was listed on the webpage it was scraped from; it then is parsed into a column for prefix (e.g. "(wb/jv)") and for project_name (e.g., ""Wikibooks Javanese")
* Source: https://incubator.wikimedia.org/wiki/Incubator:Wikis
* Manually copied (with plans to automate scraping)

### mediawiki_codesearch_rtl_true.tsv
This file contains results of [MediaWiki Codesearch](https://codesearch.wmcloud.org) query [rtl=true](https://codesearch.wmcloud.org/search/?q=rtl+%3D+true&files=Messages&excludeFiles=&repos=), showing wikis with RTL (right-to-left) text. Results were copied from the results box (result format: phabricator) and pasted into a TSV.

* Source: https://codesearch.wmcloud.org/search/?q=rtl+%3D+true&files=Messages&excludeFiles=&repos=
* Manually copied (with plans to automate scraping)

### multilingual_wikisource_language.tsv
This file contains a list of languages on Multilingual Wikisource. (i.e., these languages do not ye have their own Wikisource sub-domain.)

* Source: [Wikisource:Languages](https://wikisource.org/wiki/Wikisource:Languages)
* Manually copied (with plans to automate scraping)

### wikiversity_beta_states_of_wikiversities.tsv
This file contains entries for each Wikiversity project edition in beta (i.e., the project is still a test project; i.e., the Wikiversity edition is not yet hosted by Wikimedia with its own domain).

* Source: [Wikiversity Beta:States of Wikiversities](https://beta.wikiversity.org/wiki/States_of_Wikiversities)
* Manually copied (with plans to automate scraping)


