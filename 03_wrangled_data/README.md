### projects.tsv
This file is the output of **language.ipynb**, located in the [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/02_wrangling_scripts) folder.

This file contains a list of all current [content projects specialized by linguistic edition](https://meta.wikimedia.org/wiki/Wikimedia_projects), including those currently hosted and open, as well as those in test (i.e. in Incubator, Wikiversity Beta, or Multilingual Wikisource). Dataset includes a column for "test" vs "hosted" classification (test-hosted column) , and for "open" hosted projects and "closed" previously hosted projects (status column).

### project_languages.tsv
This file is the output of **language.ipynb**, located in the [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/gdi/incubator-data-exploration/-/tree/main/02_wrangling_scripts) folder.

This file contains a list of all wiki [content projects specialized by linguistic edition](https://meta.wikimedia.org/wiki/Wikimedia_projects) (i.e. all projects in the "projects.tsv" file) and their corresponding language names. The file includes project prefix (e.g., "Wt/es"), language name in English (e.g., "Spanish") and project type (e.g, "Spanish Wiktionary").
 
