## Files

### state_of_languages.tsv
For each language, this table shows whether the language has a hosted, test, or closed edition of the following projects: Wikibooks, Wikinews, Wikipedia, Wikiquote, Wikisource, Wikiversity, Wikivoyage, and Wiktionary.

### state_of_languages_sortable.ipynb
Using state_of_languages_with_counts_and_notes.tsv data, this notebook generates two sortable/searchable tables: one for viewing in JupterLab, and one for viewing in GitLab. For each language, the table includes counts of hosted and test projects, as well as textual summaries (e.g., "1 hosted project, 1 closed project, 3 initial test projects").

### state_of_languages_visualized.png 
A colorcoded visualization of the state_of_languages.tsv data.

### state_of_languages_with_counts_and_notes.tsv
Same data as state_of_languages.tsv, plus project counts and textual summaries (per language). This file is used in state_of_languages_sortable.ipynb

## Definitions 
In all of these files,
* "**hosted**" projects refer to projects that have their own domain and hosted by the Wikimedia Foundation (e.g., ab.wikipedia.org);
* "**test**" projects refer to projects located in one of three places:
   * Wikimedia Incubator (if Wikibooks, Wikinews, Wikipedia, Wikiquote, Wikivoyage, and Wiktionary), 
   * Wikiversity Beta, or
   * Multilingual Wikisource   
* **test (closed)**" or "**closed**" projects refer to projects that previously had their own domain hosted by the Wikimedia foundaation, but have since been closed and returned to test status; they no longer have their own domain and are located in one of the three test project locations listed above
