### /incubator

Subfolder containing Incubator-related analyses.

### /outputs

Outputs of notebooks in the current folder.

### state_of_languages_prep.ipynb

This notebook preps the data for the state_of_languages.ipynb notebook, which aims to [develop metrics for the state of languages at Wikimedia](https://meta.wikimedia.org/wiki/Research:Incubator_and_language_representation_across_Wikimedia_projects). It reads in, joins, and wrangles data from the project_languages.tsv and current_incubator_project_list.tsv files (located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data)).

* Code: Python
* Output: state_of_languages.tsv (located in [04_analysis/outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/04_analysis/outputs))


### state_of_languages.ipynb

This notebook provides stats tables and visualizations related to language representation across Wikimedia projects. It includes stats and visualizations about the number of languages with proejcts, the numbers of languages with N hosted projects, the number of languages with N test projects, etc. To run the notebook with R, you need to create a conda environment and activate the R kernel so Jupyter can recognize it.

* Code: R 
* Output: state_of_languages_visualized.png (located in [04_analysis/outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/04_analysis/outputs))

### state_of_languages-top20.ipynb

This notebook provides stats tables and visualizations related to language representation of the world's top 20 most spoken languages (as of 2023) across Wikimedia projects. To run the notebook with R, you need to create a conda environment and activate the R kernel so Jupyter can recognize it.

For this notebook and the four "..top20" notebooks below, the top 20 most spoken languages were determined via Ethnologue's 2023 "Ethnologue 200" list, from which the top 20 were listed online at https://www.ethnologue.com/insights/ethnologue200/ (archived at http://web.archive.org/web/20230309070105/https://www.ethnologue.com/insights/ethnologue200/). Citation: Eberhard, David M., Gary F. Simons, and Charles D. Fennig (eds.). 2023. Ethnologue: Languages of the World. Twenty-sixth edition. Dallas, Texas: SIL International. Online version: http://www.ethnologue.com.

Code: R 

### state_of_languages-top20_editors.ipynb 
This notebook analyzes editor statistics on projects for the top 20 most spoken languages of 2023. 

Code: R 

### state_of_languages-top20_edits.ipynb
This notebook analyzes edit activity on projects for the top 20 most spoken languages of 2023

Code: R 

### state_of_languages-top20_project_size.ipynb
This notebook analyzes project size for the hosted projects of the top 20 most spoken languages of 2023. 

Code: R 

### state_of_languages-top20_unique_devices.ipynb
This notebook analyzes project readership (i.e. devices accessing the projects) for the top 20 most spoken 

Code: R 

### qa_state_of_languages.ipynb

This notebook provides various quality assessment scripts that can be run to compare the number of wiki project versions (hosted and test) in the state_of_languages.tsv file, vs. various other locations in GitHub, Meta-Wiki, Incubator, etc. To run the notebook with R, you need to create a conda environment and activate the R kernel so Jupyter can recognize it.

Code: R 

### state_of_commons.ipynb
This notebooks analyzes language distributions for Wikimedia Commons file captions.

Code: R

### state_of_mint.ipynb
This notebooks analyzes languages translatable via Wikimedia's [MinT](https://www.mediawiki.org/wiki/MinT).

Code: R

### state_of_scripts.ipynb
This notebook analyzes scripts (i.e. writing systems) across Wikimedia projects.

Code: R 
Output: scripts_hosted_projects.csv
