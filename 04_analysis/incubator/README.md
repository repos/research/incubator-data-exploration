## *Run this first*

#### final_wrangle.ipynb 
Located in [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts?ref_type=heads) folder, this notebook generates the following outputs, sent  to the [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data?ref_type=heads) folder.

* Outputs: 
   * current_incubator_page_count.tsv: This file contains page counts for each Incubator project (current projects)
   * current_incubator_monthly_edits: This file contains monthly edits for each Incubator project (current projects)
   * current_incubator_firstlast_edits.tsv: This file contains first and last (mean) edit dates for each Incubator project (current projects)
   * incubator_graduates_firstlast_edits.tsv: This file contains first and last (mean) edit dates for each Incubator project (graduated projects)
   * substantial_projects.tsv (current substantial projects)
   * active_projects.tsv (current active projects)


## *Then the TSVs produced in the notebook above can be used in the notebooks below*

#### current_incubator_history_visualizations.ipynb
Visualizing data trends for current Incubator projects, specifically:
* Visualizing project size (i.e. how many pages do the current Incubator projects have?)
* Visualizing first and last edits (i.e., how long have current Incubator projects been in the Incubator?)
* Visualizing project size x start date (i.e., looking at projects' number of pages x how long they've been in the Incubator)
* Visualizing monthly edits 

#### current_incubator_history_visualizations-active_substantial.ipynb
Same as current_incubator_history_visualizations.ipynb, but filtered for current **substantial** and/or **active** test projects.


#### both_current_and_incubator_grad_history_visualizations.ipynb
Visualizing data trends for current and graduated Incubator projects, specifically:
* Analysis of current and graduated Incubator projects' graduation rates
* Analysis of current and graduated Incubator projects' graduated-in-less-than.. rates
* Analysis of current and graduated Incubator projects' graduated-in-x-years.. rates
* Analysis of graduation rates based on whether it's the language's 1st project, 2nd project, 3rd project, etc.
* Analyses of language's 1st project

#### both_current_and_incubator_grad_history_visualizations-active_substantial.ipynb
Same as both_current_and_incubator_grad_history_visualizations.ipynb, but filtered for current **substantial** and/or **active** test projects.

#### incubator_graduate_history_visualizations.ipynb
Visualizing data trends for graduated Incubator projects, specifically:
* Days spent in Incubator before graduation
* Gap between last edits and graduation
