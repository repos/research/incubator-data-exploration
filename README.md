# Incubator and Language Data Exploration

The purpose of this repo is to document and organize work related to the [Incubator and Language Representation Across Wikimedia Projects](https://meta.wikimedia.org/wiki/Research:Incubator_and_language_representation_across_Wikimedia_projects) research project.

Goals of this project:
* Develop metrics for the state of languages at Wikimedia
* Develop metrics for better understanding Incubator
* Develop [knowledge gaps metrics](https://meta.wikimedia.org/wiki/Research:Knowledge_Gaps_Index/Measurement) for measuring language gaps

**This repo contains code to:**
* join, clean, and wrangle the data needed to perform relevant analyses (see [02_wrangling_scripts](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts?ref_type=heads) folder)
* perform analyes of Incubator and language representation (see [04_analysis](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/04_analysis?ref_type=heads) folder)

## Repository structure
```
The structure of the repository is outlined below.
.
├── 01_source_data: Contains source files needed for running scripts
├── 02_wrangling_scripts: Contains scripts for wrangling source data files, Hive tables, etc., and creating outputs
├── 03_wrangled_data: Contains files created via the wrangling scripts
├── 04_analysis: Contains analysis notebooks re. the state of languages across Wikimedia projects
│   └── outputs: Contains files (tsv, png, etc) outputted from analysis notebooks
│   └── incubator: Contains analysis notebooks and outputs for analyses specific to Wikimedia Incubator
└── README.md
```
Each folder contains its own README providing details about the files in that folder.

## Authors and acknowledgment
Contributors: [@cmyrick](https://gitlab.wikimedia.org/cmyrick), [@hghani](https://gitlab.wikimedia.org/hghani), [@ilooremeta](https://gitlab.wikimedia.org/ilooremeta), [@kcvelaga](https://gitlab.wikimedia.org/kcvelaga)


