# Project notebooks

_These notebooks must be run in order._

_Running notebook #7 (wrangle_final.ipynb) will run all seven  notebooks in order, plus the notebooks from the "Additional notebooks" section below._

### 1. closed_projects.ipynb
This notebook generates a dataset of closed Wikimedia projects. Closed projects refer to wikis hosted by the Wikimedia Foundation that have been closed (or in some cases deleted) per the closure process define by the Language committee. Learn more here: https://meta.wikimedia.org/wiki/Closing_projects_policy

The dataset is generated by wrangling the [closure_proposals.xlsx](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/01_source_data/closure_proposals.xlsx) file (located in the [01_source_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/01_source_data) folder), which was crteated by manually scraping the [Proposals for closing projects](https://meta.wikimedia.org/wiki/Proposals_for_closing_projects#Closed_proposals) and the [Proposals for closing projects/Archive](https://meta.wikimedia.org/wiki/Proposals_for_closing_projects/Archive) tables; then combining with extra data from Canonical Data list; and then manually adding missing data, with explanations.

* Code: Python
* Output produced: **closed_projects.csv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder

### 2. site_creation_scraper.ipynb
This scraper scrapes graduated Incubator project names and graduation dates from the Wikipedia Incubator's [site creation log](https://incubator.wikimedia.org/wiki/Incubator:Site_creation_log).
* Code: Python
* Output: **site_creation.tsv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder

### 3. language.ipynb
This notebooks generates a dataframe matching all Incubator project prefixes (e.g, "Wt/ig") with its corresponding language name and project type (e.g., "Igbo Wiktionary"). It does this by matching langauge codes with [canonical-data.wikis](https://github.com/wikimedia-research/canonical-data/blob/master/wiki/wikis.tsv) language codes, and then matching remaning codes with [SIL IS0 639-3s](https://iso639-3.sil.org/sites/iso639-3/files/downloads/iso-639-3.tab).
* Code: Python
* Output: **projects.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
* Output: **project_languages.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder

### 4. requests_for_new_languages.ipynb
This notebook scrapes the tables from https://meta.wikimedia.org/wiki/Requests_for_new_languages. The data are then combined, filtered for only projects with status: **"Verified for eligibility"**, wrangled, joined with language codes, and exported as TSV.
* Code: Python
* Output: **new_language_requests_verified.tsv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder

### 5. incubator_queries_for_analysis.ipynb
(Formerly called "load_dataframe_for_analysis.ipynb") This notebook generates datasets and exports TSVs containing page count data and edits data for current and past Incubator projects. It also generates lists of active and substantial Incubator project prefixes and exports them as TSVs. 
* Code: Python
* Outputs 
   * **current_incubator_page_count.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
   * **current_incubator_monthly_edits.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
   * **current_incubator_firstlast_edits.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
   * **incubator_graduates_firstlast_edits.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
   * **incubator_active_projects.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
   * **incubator_substantial_projects.tsv** located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder

### 6. qa_incubator_queries_for_analysis.ipynb
This notebook looks for non-matches between the **projects.tsv** and the TSV outputs from the **incubator_queries_for_analysis.ipynb** notebook. Once needed removals are determined, the relevent files (e.g., projects.tsv, project_languages.tsv) are re-exported/overwritten.
* Code: R
* Output: **current_removals.csv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder
* Output: **grad_removals.csv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder

### 7. wrangle_final.ipynb
**This notebook runs all of the preceding notebooks (1-5), and then overrides the projects.tsv and project_languages.tsv file based on the QA applied in the previous notebook (5).**

* Code: R
* Output: **projects.tsv** (overwritten) located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder
* Output: **project_languages.tsv** (overwritten) located in [03_wrangled_data](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/03_wrangled_data) folder


# Additional notebooks 

_These notebooks do not need to be run in order, and will be automatically run via the analysis notebooks that use them._

### scripts_scraper.ipynb
This notebook scrapes and wrangles Wikimedia script (i.e. writing system) data

* Code: R
* Output: scripts.tsv located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder


### rtl_languages.ipynb
This notebook wrangles the list of right-to-left languages from the the mediawiki_codesearch_rtl_true.tsv file

* Code: R
* Input: mediawiki_codesearch_rtl_true.tsv
* Output: **rtl_languages.tsv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder

### mint_wrangling.ipynb
This notebook wrangles the list of languages that have MinT translation availability.

* Code: R
* Data source: https://phabricator.wikimedia.org/diffusion/GCXS/browse/master/config/MinT.yaml
* Output: **mint_languages.tsv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder
* Output: **mint_language_pairs.tsv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder

### commons_caption_languages.ipynb
This notebook gets language counts for Wikimedia Commons file captions 

* Code: Python 
* Data source: https://dumps.wikimedia.org/other/wikibase/commonswiki/
* Output: **commons_caption_language_counts_(standardized).tsv** located in [02_wrangled_data/temp_outputs](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/tree/main/02_wrangling_scripts/temp_outputs) folder


