### closed_projects.csv
This file contains closed wikimedia projects, closure proposal dates, closure or deletion date, projects language, and project type. The data were parsed from closure_proposals.xlsx (from 01_source_data folder) and combined with extra data from the canonical-data wiki list; then, missing data added manually.
* Wrangled via [closed_projects.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/closed_projects.ipynb)

### site_creataion.tsv
This file contains a list of all wikis that "graduated" from Incubator, Wikiverisity Beta, or Multilingual Wikisource (i.e. received their own site/domain) since June 2006; the list includes the graduation date (i.e. site creation date).
* Source: [Incubator:Site_creation_log](https://incubator.wikimedia.org/wiki/Incubator:Site_creation_log)
* Scraped via [site_creation_scraper.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/site_creation_scraper.ipynb?ref_type=heads)
* Manually cleaned

### qa_....tsv files
The files contain lists of current projects and graduated projects (i.e. prefixes) that should be removed from or added to the projects.tsv and project_languages.tsv files
* Generated via [qa_incubator_queries_for_analysis.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/qa_incubator_queries_for_analysis.ipynb)
* To be executed in [wrangle_final.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/wrangle_final.ipynb)

### commons_caption_language_counts.tsv	 
Wrangled via [commons_caption_languages.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/commons_caption_languages.ipynb)

### commons_caption_language_counts_(standardized).tsv 
Wrangled via [commons_caption_languages.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/commons_caption_languages.ipynb)

### mint_language_pairs.csv	 

### mint_languages.csv 

### requests_for_new_languages-verified_and_discussion.tsv	
Scraped and wrangled via [requests_for_new_languages_scraper.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/requests_for_new_languages_scraper.ipynb)

### rtl_languages.tsv	 
Wrangled via [rtl_languages.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/rtl_languages.ipynb)

### scripts.tsv
Scraped and wrangled via [scripts_scraper.ipynb](https://gitlab.wikimedia.org/repos/research/incubator-data-exploration/-/blob/main/02_wrangling_scripts/scripts_scraper.ipynb)
